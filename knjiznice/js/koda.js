var i=1;

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = stPacienta.ehrId;
  if(stPacienta=1) {
    var partyData={
      firstNames: "Janez",
      lastNames: "Kranjski",
      dateOfBirth: "1978-9-22T00:00",
      partyAdditionalInfo: [
        {
          key: "ehrId",
          value: ehrId  
        }
      ]
    };
  };
  if(stPacienta=2) {
    var partyData={
      firstNames: "Lojze",
      lastNames: "Svetokriški",
      dateOfBirth: "1987-6-11T20:50",
      partyAdditionalInfo: [
        {
          key: "ehrId",
          value: ehrId  
        }
      ]
    };
  };
  if(stPacienta=3) {
    var partyData={
      firstNames: "Tom",
      lastNames: "Tomos",
      dateOfBirth: "2004-2-28T13:37",
      partyAdditionalInfo: [
        {
          key: "ehrId",
          value: ehrId  
        }
      ]
    };
  };
  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
if(i<3) {
  window.onload=function() {
  document.getElementById("dataGen").addEventListener("click", generirajPodatke(i));
  document.getElementById("dataGen").addEventListener("click", function(i) {
    i++;
  });
  }
}